package cordova.plugin.containertextimage_preprocessing;

import android.util.Base64;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class ContainerTextImage_PreProcessing extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("Preprocess")) {
            String message = args.getString(0);
            this.Preprocess(message, callbackContext);
            return true;
        }
        return false;
    }

    private void Preprocess(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            String result = Base64.encodeToString(Base64.decode(message, Base64.DEFAULT), Base64.DEFAULT);
            callbackContext.success(result);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

}
