var exec = require('cordova/exec');

module.exports.Preprocess = function (arg0, success, error) {
    exec(success, error, 'ContainerTextImage_PreProcessing', 'Preprocess', [arg0]);
};
